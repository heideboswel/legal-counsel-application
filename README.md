# Heidemari Boswel
Pretoria, South Africa · [linkedIn](https://www.linkedin.com/in/heidemari-boswel/)

2 June 2022

Dear GitLab team,

I am writing to apply for the Legal Counsel II, Privacy (EMEA) position (although I think Legal Counsel will be more appropriate). 

The role is very appealing to me, and I believe that my strong legal experience and education make me a highly competitive candidate for this position. My key strengths that would support my success in this position include:

* A detail oriented mindset.
* I am comfortable working under pressure.
* I am a team player who enjoys collaborating with colleagues.
* I am an eager learner who welcomes any new learning opportunity with vigour.

With a B.Com (Law) degree and LLB degree, I have a broad understanding of legal matters. I also have experience in learning and applying new technologies as appropriate. I am currently busy with the web developer bootcamp course on Udemy in order to get a better feel for the technology industry and to further develop my skillset.

Please see my resume for additional information on my experience.

Thank you for your time and consideration. I look forward to speaking with you about this employment opportunity.


Sincerely,

Heidemari Boswel


